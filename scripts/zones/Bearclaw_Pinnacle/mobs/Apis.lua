------------------------------------
-- Area: Bearclaw Pinnacle
--  Mob: Apis
--  ENM: Holy Cow
-----------------------------------
require("scripts/globals/status")
-----------------------------------

function onMobInitialize(mob)
    mob:setMod(tpz.mod.GRAVITYRES, 75)
    mob:setMod(tpz.mod.BINDRES, 75)
    mob:setMod(tpz.mod.SLEEPRES, 75)
end

function onMobSpawn(mob)
	mob:addStatusEffect(tpz.effect.REGAIN,10,3,0);
end

function onMobFight(mob)
	-- Animations for the different phases
	local battletime = mob:getBattleTime()
	local twohourTime = mob:getLocalVar("twohourTime")

	if twohourTime == 0 then
	    mob:setLocalVar("twohourTime",math.random(30,90))
	end

	if battletime >= twohourTime then
	    local phaseChance = math.random(0, 99)
	    mob:setLocalVar("twohourTime",battletime + 30) -- Phase lasts for 30 seconds
	    
		if (phaseChance >= 0 and phaseChance < 35) then -- Blue 2hr = high dmg output, double/triple attack, no magic dmg taken, full physical dmg taken
			mob:weaknessTrigger(0)
			mob:setDamage(425) -- overwrites base stats so that Apis hits for 600+ on a nin tank
			mob:setMod(tpz.mod.DOUBLE_ATTACK, 60)
			mob:setMod(tpz.mod.TRIPLE_ATTACK, 30)
			mob:setMod(tpz.mod.UDMGPHYS, 100);
			mob:setMod(tpz.mod.UDMGMAGIC, -100);
		elseif (phaseChance >= 35 and phaseChance < 75) then -- Green 2hr = moderate att dmg output, takes partial magic + physical dmg but not full dmg
			mob:weaknessTrigger(1)
			mob:setDamage(140)
			mob:setMod(tpz.mod.DOUBLE_ATTACK, 0)
			mob:setMod(tpz.mod.TRIPLE_ATTACK, 0)
			mob:setMod(tpz.mod.UDMGPHYS, -25);
			mob:setMod(tpz.mod.UDMGMAGIC, -35);
		elseif (phaseChance >= 75 and phaseChance < 100) then -- Red 2hr = takes very little physical dmg output, full magic dmg taken, high physical dmg resist
			mob:weaknessTrigger(2)
			mob:setDamage(60)
			mob:setMod(tpz.mod.DOUBLE_ATTACK, 0)
			mob:setMod(tpz.mod.TRIPLE_ATTACK, 0)
			mob:setMod(tpz.mod.UDMGPHYS, -90);
			mob:setMod(tpz.mod.UDMGMAGIC, 100);
		else
			mob:setDamage(100) -- default but shouldnt be reachable normally
		end
	end
end

function onMobWeaponSkill(target, mob, skill)
	if (skill:getID() == 495) then -- Snort
		mob:resetEnmity(target)
	end
end

function onMobDeath(mob)
end