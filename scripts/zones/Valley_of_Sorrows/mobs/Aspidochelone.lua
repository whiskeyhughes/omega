-----------------------------------
-- Area: Valley of Sorrows
--  HNM: Aspidochelone
-- !pos 19 1 14 128
-----------------------------------
local ID = require("scripts/zones/Valley_of_Sorrows/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobInitialize(mob)
    mob:setMod(tpz.mod.SLEEPRES, 75)
    mob:setMod(tpz.mod.LULLABYRES, 75)
    mob:addMod(tpz.mod.EARTHRES, 75)
end

function onMobSpawn(mob)
    mob:AnimationSub(0)
    mob:setMod(tpz.mod.UDMGPHYS, 0)
    mob:setMod(tpz.mod.REGEN, 0)
    mob:setLocalVar("totalDMGTowardsPhaseChange", 0)

    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.ADAMANTOISE_QM):setStatus(tpz.status.DISAPPEAR)
    end

    mob:setLocalVar("[rage]timer", 3600) -- 60 minutes
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.ASPIDOCHELONE_SINKER)
end

function onMobFight(mob)
    local dmgTaken = mob:getLocalVar("totalDMGTowardsPhaseChange")
    print(dmgTaken)
    if (dmgTaken >= 10000) then
        mob:setLocalVar("totalDMGTowardsPhaseChange", 0)
        if (mob:AnimationSub() == 0) then
            mob:AnimationSub(1) -- Withdraw into shell
            mob:setMod(tpz.mod.UDMGPHYS, -90)
            mob:setMod(tpz.mod.REGEN, mob:getMaxHP()*.01)
        elseif (mob:AnimationSub() == 1) then
            mob:AnimationSub(0) -- Come out of shell
            mob:setMod(tpz.mod.UDMGPHYS, 0)
            mob:setMod(tpz.mod.REGEN, 0)
        end
    end
end

function onMobRoam(mob) -- reset back to defauls on a wipe
    mob:AnimationSub(0)
    mob:setMod(tpz.mod.UDMGPHYS, 0)
    mob:setMod(tpz.mod.REGEN, 0)
    mob:setLocalVar("totalDMGTowardsPhaseChange", 0)
end

function onMobDespawn(mob)
    -- Set Aspidochelone's Window Open Time
    if LandKingSystem_HQ ~= 1 then
        local wait = 72 * 3600
        SetServerVariable("[POP]Aspidochelone", os.time() + wait) -- 3 days
        if LandKingSystem_HQ == 0 then -- Is time spawn only
            DisallowRespawn(mob:getID(), true)
        end
    end

    -- Set Adamantoise's spawnpoint and respawn time (21-24 hours)
    if LandKingSystem_NQ ~= 1 then
        SetServerVariable("[PH]Aspidochelone", 0)
        DisallowRespawn(ID.mob.ADAMANTOISE, false)
        UpdateNMSpawnPoint(ID.mob.ADAMANTOISE)
        GetMobByID(ID.mob.ADAMANTOISE):setRespawnTime(75600 + math.random(0, 6) * 1800) -- 21 - 24 hours with half hour windows
    end
end

-- Build Sleep resistance
function onMagicHit(caster, target, spell)
    local numSleeps = target:getLocalVar("numSleeps")
    if (hasSleepEffects(target) and (spell:getID() == 253 or spell:getID() == 259 or spell:getID() == 273 or spell:getID() == 274 or spell:getID() == 376 or spell:getID() == 463)) then
        
        local lastSleep = target:getLocalVar("lastSleep")
        if (target:getBattleTime() >= lastSleep) then
            if (numSleeps > 3) then
                target:setMod(tpz.mod.SLEEPRES, 500)
                target:setMod(tpz.mod.LULLABYRES, 500)
            end
            target:setLocalVar("numSleeps", numSleeps + 1)

            -- Figure out how long the current sleep is supposed to last so we don't increase
            -- the sleep resist too soon
            local duration = 0
            if (target:hasStatusEffect(tpz.effect.SLEEP_I)) then
                duration = target:getStatusEffect(tpz.effect.SLEEP_I):getDuration()
            elseif (target:hasStatusEffect(tpz.effect.SLEEP_II)) then
                duration = target:getStatusEffect(tpz.effect.SLEEP_II):getDuration()
            elseif (target:hasStatusEffect(tpz.effect.LULLABY)) then
                duration = target:getStatusEffect(tpz.effect.LULLABY):getDuration()
            end
            target:setLocalVar("lastSleep", target:getBattleTime() + duration)
        end
    else
        if (not hasSleepEffects(target)) then
            target:setLocalVar("lastSleep", 0)
        end
    end
end
