﻿<#

.SYNOPSIS

Creates Topaz.exe shortcuts with names unique unto the map the executable launches.

.DESCRIPTION

Names of the executables are defined in the array $TopazMapExecutables

A ForEach statement is leverage to create a new executable based on the names in the $TopazMapExecutables string array.

.LINK

https://i.imgflip.com/4j7ckc.jpg

.NOTES

Author: Stephen Simon

Created: 06/10/21

#>

 

#####Create Function to Remove Values of All Previously Stored Varibles#####

Function Remove-ValuesofPreviouslyStoredVariables {

    Remove-Variable * -ErrorAction SilentlyContinue

}

 

#####Create Function to Check Status of Topaz Connect#####

Function Copy-TopazExecutable {

    $TopazMapExecutables = @('topaz_airships.exe', 'topaz_Aragoneu.exe', 'topaz_Arrapago.exe', 'topaz_Alzadaal.exe', 'topaz_Bastok.exe', 'topaz_Derfland.exe', 'topaz_Dynamis.exe', 'topaz_ELowlands.exe', 'topaz_EUplands.exe', 'topaz_Emptiness.exe', 'topaz_Fauregandi.exe', 'topaz_Gustaberg.exe', 'topaz_Halvung.exe', 'topaz_Jeuno.exe', 'topaz_Kolshushu.exe', 'topaz_Kuzotz.exe', 'topaz_Litelor.exe', 'topaz_Lumoria.exe', 'topaz_Mamool.exe', 'topaz_Misc.exe', 'topaz_Movalpolis.exe', 'topaz_Norvallen.exe', 'topaz_Qufim.exe', 'topaz_Ronfaure.exe', 'topaz_Sandoria.exe', 'topaz_Sarutabaruta.exe', 'topaz_Tavnazian.exe', 'topaz_Tulia.exe', 'topaz_Valdeaunia.exe', 'topaz_Vollbow.exe', 'topaz_Whitegate.exe', 'topaz_Windurst.exe', 'topaz_Zulkheim.exe')

    ForEach ($Executable in $TopazMapExecutables) {

        Copy-Item -Path "C:\Omega\Omega\topaz_game_64.exe" -Destination "C:\Omega\Omega\out\$Executable"

    }

}

 

Remove-ValuesofPreviouslyStoredVariables

Copy-TopazExecutable