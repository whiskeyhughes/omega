﻿<#
.SYNOPSIS
Checks the status of the three processes required to run a Final Fantasy XI server.
.DESCRIPTION
Test-topaz_new_connect_64 checks the status of the topaz_new_connect_64.exe process. It will start it if it does not detect it running.
Test-topaz_ checks the status of the topaz_.exe process. It will start it if it does not detect it running.
Test-topaz_search_64 checks the status of the topaz_search_64.exe process. It will start it if it does not detect it running.
.LINK
https://i.imgflip.com/4j7ckc.jpg
.NOTES
Author: Joshua Hughes and Stephen Simon
Created: 04/25/21
#>

#####Create Function to Remove Values of All Previously Stored Varibles#####
Function Remove-ValuesofPreviouslyStoredVariables {
    Remove-Variable * -ErrorAction SilentlyContinue
}

#####Create Function to Check Status of Topaz Connect#####
Function Test-topaz_new_connect_64 {
    $Statusoftopaz_new_connect_64 = (Get-Process | Where-Object ProcessName -EQ "topaz_new_connect_64").Count
    If ($Statusoftopaz_new_connect_64 -EQ "1") {
        Write-Host "The process topaz_new_connect_64 is running!"
    }
    ElseIf ($Statusoftopaz_new_connect_64 -NE "1") {
        Write-Host "The process topaz_new_connect_64 is NOT running!"
        Start-Process -FilePath "topaz_new_connect_64.exe" -WorkingDirectory "C:\Omega\Omega\"
    }
}

#####Create Function to Check Status of Airships#####
Function Test-Topaz_Airships {
    $StatusofTopaz_airships = (Get-Process | Where-Object ProcessName -EQ "topaz_airships").Count
    If ($StatusofTopaz_airships -EQ "1") {
        Write-Host "The process topaz_airships is running!"
    }
    ElseIf ($StatusofTopaz_airships -NE "1") {
        Write-Host "The process topaz_airships is NOT running!"
        Start-Process -FilePath "topaz_airships.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54230 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Aragoneu#####
Function Test-Topaz_Aragoneu {
    $StatusofTopaz_Aragoneu = (Get-Process | Where-Object ProcessName -EQ "topaz_Aragoneu").Count
    If ($StatusofTopaz_Aragoneu -EQ "1") {
        Write-Host "The process topaz_Aragoneu is running!"
    }
    ElseIf ($StatusofTopaz_Aragoneu -NE "1") {
        Write-Host "The process topaz_Aragoneu is NOT running!"
        Start-Process -FilePath "topaz_Aragoneu.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54231 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Arrapago#####
Function Test-Topaz_Arrapago {
    $StatusofTopaz_Arrapago = (Get-Process | Where-Object ProcessName -EQ "topaz_Arrapago").Count
    If ($StatusofTopaz_Arrapago -EQ "1") {
        Write-Host "The process topaz_Arrapago is running!"
    }
    ElseIf ($StatusofTopaz_Arrapago -NE "1") {
        Write-Host "The process topaz_Arrapago is NOT running!"
        Start-Process -FilePath "topaz_Arrapago.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54232 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Alzadaal#####
Function Test-Topaz_Alzadaal {
    $StatusofTopaz_Alzadaal = (Get-Process | Where-Object ProcessName -EQ "topaz_Alzadaal").Count
    If ($StatusofTopaz_Alzadaal -EQ "1") {
        Write-Host "The process topaz_Alzadaal is running!"
    }
    ElseIf ($StatusofTopaz_Alzadaal -NE "1") {
        Write-Host "The process topaz_Alzadaal is NOT running!"
        Start-Process -FilePath "topaz_Alzadaal.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54253 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Bastok#####
Function Test-Topaz_Bastok {
    $StatusofTopaz_Bastok = (Get-Process | Where-Object ProcessName -EQ "topaz_Bastok").Count
    If ($StatusofTopaz_Bastok -EQ "1") {
        Write-Host "The process topaz_Bastok is running!"
    }
    ElseIf ($StatusofTopaz_Bastok -NE "1") {
        Write-Host "The process topaz_Bastok is NOT running!"
        Start-Process -FilePath "topaz_Bastok.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54233 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Derfland#####
Function Test-Topaz_Derfland {
    $StatusofTopaz_Derfland = (Get-Process | Where-Object ProcessName -EQ "topaz_Derfland").Count
    If ($StatusofTopaz_Derfland -EQ "1") {
        Write-Host "The process topaz_Derfland is running!"
    }
    ElseIf ($StatusofTopaz_Derfland -NE "1") {
        Write-Host "The process topaz_Derfland is NOT running!"
        Start-Process -FilePath "topaz_Derfland.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54234 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Dynamis#####
Function Test-Topaz_Dynamis {
    $StatusofTopaz_Dynamis = (Get-Process | Where-Object ProcessName -EQ "topaz_Dynamis").Count
    If ($StatusofTopaz_Dynamis -EQ "1") {
        Write-Host "The process topaz_Dynamis is running!"
    }
    ElseIf ($StatusofTopaz_Dynamis -NE "1") {
        Write-Host "The process topaz_Dynamis is NOT running!"
        Start-Process -FilePath "topaz_Dynamis.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54235 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of ELowlands#####
Function Test-Topaz_ELowlands {
    $StatusofTopaz_ELowlands = (Get-Process | Where-Object ProcessName -EQ "topaz_ELowlands").Count
    If ($StatusofTopaz_ELowlands -EQ "1") {
        Write-Host "The process topaz_ELowlands is running!"
    }
    ElseIf ($StatusofTopaz_ELowlands -NE "1") {
        Write-Host "The process topaz_ELowlands is NOT running!"
        Start-Process -FilePath "topaz_ELowlands.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54236 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of EUplands#####
Function Test-Topaz_EUplands {
    $StatusofTopaz_EUplands = (Get-Process | Where-Object ProcessName -EQ "topaz_EUplands").Count
    If ($StatusofTopaz_EUplands -EQ "1") {
        Write-Host "The process topaz_EUplands is running!"
    }
    ElseIf ($StatusofTopaz_EUplands -NE "1") {
        Write-Host "The process topaz_EUplands is NOT running!"
        Start-Process -FilePath "topaz_EUplands.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54237 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Emptiness#####
Function Test-Topaz_Emptiness {
    $StatusofTopaz_Emptiness = (Get-Process | Where-Object ProcessName -EQ "topaz_Emptiness").Count
    If ($StatusofTopaz_Emptiness -EQ "1") {
        Write-Host "The process topaz_Emptiness is running!"
    }
    ElseIf ($StatusofTopaz_Emptiness -NE "1") {
        Write-Host "The process topaz_Emptiness is NOT running!"
        Start-Process -FilePath "topaz_Emptiness.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54238 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Fauregandi#####
Function Test-Topaz_Fauregandi {
    $StatusofTopaz_Fauregandi = (Get-Process | Where-Object ProcessName -EQ "topaz_Fauregandi").Count
    If ($StatusofTopaz_Fauregandi -EQ "1") {
        Write-Host "The process topaz_Fauregandi is running!"
    }
    ElseIf ($StatusofTopaz_Fauregandi -NE "1") {
        Write-Host "The process topaz_Fauregandi is NOT running!"
        Start-Process -FilePath "topaz_Fauregandi.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54239 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Gustaberg#####
Function Test-Topaz_Gustaberg {
    $StatusofTopaz_Gustaberg = (Get-Process | Where-Object ProcessName -EQ "topaz_Gustaberg").Count
    If ($StatusofTopaz_Gustaberg -EQ "1") {
        Write-Host "The process topaz_Gustaberg is running!"
    }
    ElseIf ($StatusofTopaz_Gustaberg -NE "1") {
        Write-Host "The process topaz_Gustaberg is NOT running!"
        Start-Process -FilePath "topaz_Gustaberg.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54240 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Halvung#####
Function Test-Topaz_Halvung {
    $StatusofTopaz_Halvung = (Get-Process | Where-Object ProcessName -EQ "topaz_Halvung").Count
    If ($StatusofTopaz_Halvung -EQ "1") {
        Write-Host "The process topaz_Halvung is running!"
    }
    ElseIf ($StatusofTopaz_Halvung -NE "1") {
        Write-Host "The process topaz_Halvung is NOT running!"
        Start-Process -FilePath "topaz_Halvung.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54241 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Jeuno#####
Function Test-Topaz_Jeuno {
    $StatusofTopaz_Jeuno = (Get-Process | Where-Object ProcessName -EQ "topaz_Jeuno").Count
    If ($StatusofTopaz_Jeuno -EQ "1") {
        Write-Host "The process topaz_Jeuno is running!"
    }
    ElseIf ($StatusofTopaz_Jeuno -NE "1") {
        Write-Host "The process topaz_Jeuno is NOT running!"
        Start-Process -FilePath "topaz_Jeuno.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54242 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Kolshushu#####
Function Test-Topaz_Kolshushu {
    $StatusofTopaz_Kolshushu = (Get-Process | Where-Object ProcessName -EQ "topaz_Kolshushu").Count
    If ($StatusofTopaz_Kolshushu -EQ "1") {
        Write-Host "The process topaz_Kolshushu is running!"
    }
    ElseIf ($StatusofTopaz_Kolshushu -NE "1") {
        Write-Host "The process topaz_Kolshushu is NOT running!"
        Start-Process -FilePath "topaz_Kolshushu.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54243 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Kuzotz#####
Function Test-Topaz_Kuzotz {
    $StatusofTopaz_Kuzotz = (Get-Process | Where-Object ProcessName -EQ "topaz_Kuzotz").Count
    If ($StatusofTopaz_Kuzotz -EQ "1") {
        Write-Host "The process topaz_Kuzotz is running!"
    }
    ElseIf ($StatusofTopaz_Kuzotz -NE "1") {
        Write-Host "The process topaz_Kuzotz is NOT running!"
        Start-Process -FilePath "topaz_Kuzotz.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54244 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Litelor#####
Function Test-Topaz_Litelor {
    $StatusofTopaz_Litelor = (Get-Process | Where-Object ProcessName -EQ "topaz_Litelor").Count
    If ($StatusofTopaz_Litelor -EQ "1") {
        Write-Host "The process topaz_Litelor is running!"
    }
    ElseIf ($StatusofTopaz_Litelor -NE "1") {
        Write-Host "The process topaz_Litelor is NOT running!"
        Start-Process -FilePath "topaz_Litelor.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54245 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Lumoria#####
Function Test-Topaz_Lumoria {
    $StatusofTopaz_Lumoria = (Get-Process | Where-Object ProcessName -EQ "topaz_Lumoria").Count
    If ($StatusofTopaz_Lumoria -EQ "1") {
        Write-Host "The process topaz_Lumoria is running!"
    }
    ElseIf ($StatusofTopaz_Lumoria -NE "1") {
        Write-Host "The process topaz_Lumoria is NOT running!"
        Start-Process -FilePath "topaz_Lumoria.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54246 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Mamool#####
Function Test-Topaz_Mamool {
    $StatusofTopaz_Mamool = (Get-Process | Where-Object ProcessName -EQ "topaz_Mamool").Count
    If ($StatusofTopaz_Mamool -EQ "1") {
        Write-Host "The process topaz_Mamool is running!"
    }
    ElseIf ($StatusofTopaz_Mamool -NE "1") {
        Write-Host "The process topaz_Mamool is NOT running!"
        Start-Process -FilePath "topaz_Mamool.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54247 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Misc#####
Function Test-Topaz_Misc {
    $StatusofTopaz_Misc = (Get-Process | Where-Object ProcessName -EQ "topaz_Misc").Count
    If ($StatusofTopaz_Misc -EQ "1") {
        Write-Host "The process topaz_Misc is running!"
    }
    ElseIf ($StatusofTopaz_Misc -NE "1") {
        Write-Host "The process topaz_Misc is NOT running!"
        Start-Process -FilePath "topaz_Misc.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54248 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Movalpolis#####
Function Test-Topaz_Movalpolis {
    $StatusofTopaz_Movalpolis = (Get-Process | Where-Object ProcessName -EQ "topaz_Movalpolis").Count
    If ($StatusofTopaz_Movalpolis -EQ "1") {
        Write-Host "The process topaz_Movalpolis is running!"
    }
    ElseIf ($StatusofTopaz_Movalpolis -NE "1") {
        Write-Host "The process topaz_Movalpolis is NOT running!"
        Start-Process -FilePath "topaz_Movalpolis.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54249 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Norvallen#####
Function Test-Topaz_Norvallen {
    $StatusofTopaz_Norvallen = (Get-Process | Where-Object ProcessName -EQ "topaz_Norvallen").Count
    If ($StatusofTopaz_Norvallen -EQ "1") {
        Write-Host "The process topaz_Norvallen is running!"
    }
    ElseIf ($StatusofTopaz_Norvallen -NE "1") {
        Write-Host "The process topaz_Norvallen is NOT running!"
        Start-Process -FilePath "topaz_Norvallen.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54250 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Qufim#####
Function Test-Topaz_Qufim {
    $StatusofTopaz_Qufim = (Get-Process | Where-Object ProcessName -EQ "topaz_Qufim").Count
    If ($StatusofTopaz_Qufim -EQ "1") {
        Write-Host "The process topaz_Qufim is running!"
    }
    ElseIf ($StatusofTopaz_Qufim -NE "1") {
        Write-Host "The process topaz_Qufim is NOT running!"
        Start-Process -FilePath "topaz_Qufim.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54251 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Ronfaure#####
Function Test-Topaz_Ronfaure {
    $StatusofTopaz_Ronfaure = (Get-Process | Where-Object ProcessName -EQ "topaz_Ronfaure").Count
    If ($StatusofTopaz_Ronfaure -EQ "1") {
        Write-Host "The process topaz_Ronfaure is running!"
    }
    ElseIf ($StatusofTopaz_Ronfaure -NE "1") {
        Write-Host "The process topaz_Ronfaure is NOT running!"
        Start-Process -FilePath "topaz_Ronfaure.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54252 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Sandoria#####
Function Test-Topaz_Sandoria {
    $StatusofTopaz_Sandoria = (Get-Process | Where-Object ProcessName -EQ "topaz_Sandoria").Count
    If ($StatusofTopaz_Sandoria -EQ "1") {
        Write-Host "The process topaz_Sandoria is running!"
    }
    ElseIf ($StatusofTopaz_Sandoria -NE "1") {
        Write-Host "The process topaz_Sandoria is NOT running!"
        Start-Process -FilePath "topaz_Sandoria.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54254 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Sarutabaruta#####
Function Test-Topaz_Sarutabaruta {
    $StatusofTopaz_Sarutabaruta = (Get-Process | Where-Object ProcessName -EQ "topaz_Sarutabaruta").Count
    If ($StatusofTopaz_Sarutabaruta -EQ "1") {
        Write-Host "The process topaz_Sarutabaruta is running!"
    }
    ElseIf ($StatusofTopaz_Sarutabaruta -NE "1") {
        Write-Host "The process topaz_Sarutabaruta is NOT running!"
        Start-Process -FilePath "topaz_Sarutabaruta.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54255 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Tavnazian#####
Function Test-Topaz_Tavnazian {
    $StatusofTopaz_Tavnazian = (Get-Process | Where-Object ProcessName -EQ "topaz_Tavnazian").Count
    If ($StatusofTopaz_Tavnazian -EQ "1") {
        Write-Host "The process topaz_Tavnazian is running!"
    }
    ElseIf ($StatusofTopaz_Tavnazian -NE "1") {
        Write-Host "The process topaz_Tavnazian is NOT running!"
        Start-Process -FilePath "topaz_Tavnazian.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54256 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Tulia#####
Function Test-Topaz_Tulia {
    $StatusofTopaz_Tulia = (Get-Process | Where-Object ProcessName -EQ "topaz_Tulia").Count
    If ($StatusofTopaz_Tulia -EQ "1") {
        Write-Host "The process topaz_Tulia is running!"
    }
    ElseIf ($StatusofTopaz_Tulia -NE "1") {
        Write-Host "The process topaz_Tulia is NOT running!"
        Start-Process -FilePath "topaz_Tulia.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54257 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Valdeaunia#####
Function Test-Topaz_Valdeaunia {
    $StatusofTopaz_Valdeaunia = (Get-Process | Where-Object ProcessName -EQ "topaz_Valdeaunia").Count
    If ($StatusofTopaz_Valdeaunia -EQ "1") {
        Write-Host "The process topaz_Valdeaunia is running!"
    }
    ElseIf ($StatusofTopaz_Valdeaunia -NE "1") {
        Write-Host "The process topaz_Valdeaunia is NOT running!"
        Start-Process -FilePath "topaz_Valdeaunia.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54258 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Vollbow#####
Function Test-Topaz_Vollbow {
    $StatusofTopaz_Vollbow = (Get-Process | Where-Object ProcessName -EQ "topaz_Vollbow").Count
    If ($StatusofTopaz_Vollbow -EQ "1") {
        Write-Host "The process topaz_Vollbow is running!"
    }
    ElseIf ($StatusofTopaz_Vollbow -NE "1") {
        Write-Host "The process topaz_Vollbow is NOT running!"
        Start-Process -FilePath "topaz_Vollbow.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54259 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Whitegate#####
Function Test-Topaz_Whitegate {
    $StatusofTopaz_Whitegate = (Get-Process | Where-Object ProcessName -EQ "topaz_Whitegate").Count
    If ($StatusofTopaz_Whitegate -EQ "1") {
        Write-Host "The process topaz_Whitegate is running!"
    }
    ElseIf ($StatusofTopaz_Whitegate -NE "1") {
        Write-Host "The process topaz_Whitegate is NOT running!"
        Start-Process -FilePath "topaz_Whitegate.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54260 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Windurst#####
Function Test-Topaz_Windurst {
    $StatusofTopaz_Windurst = (Get-Process | Where-Object ProcessName -EQ "topaz_Windurst").Count
    If ($StatusofTopaz_Windurst -EQ "1") {
        Write-Host "The process topaz_Windurst is running!"
    }
    ElseIf ($StatusofTopaz_Windurst -NE "1") {
        Write-Host "The process topaz_Windurst is NOT running!"
        Start-Process -FilePath "topaz_Windurst.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54261 --ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Check Status of Zulkheim#####
Function Test-Topaz_Zulkheim {
    $StatusofTopaz_Zulkheim = (Get-Process | Where-Object ProcessName -EQ "topaz_Zulkheim").Count
    If ($StatusofTopaz_Zulkheim -EQ "1") {
        Write-Host "The process topaz_Zulkheim is running!"
    }
    ElseIf ($StatusofTopaz_Zulkheim -NE "1") {
        Write-Host "The process topaz_Zulkheim is NOT running!"
        Start-Process -FilePath "topaz_Zulkheim.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--port 54262 --ip PUBLIC IP ADDRESS"
    }
}




#####Create Function to Check Status of Topaz Search#####
Function Test-topaz_search_64 {
    $Statusoftopaz_search_64 = (Get-Process | Where-Object ProcessName -EQ "topaz_search_64").Count
    If ($Statusoftopaz_search_64 -EQ "1") {
        Write-Host "The process topaz_search_64 is running!"
    }
    ElseIf ($Statusoftopaz_search_64 -NE "1") {
        Write-Host "The process topaz_search_64 is NOT running!"
        Start-Process -FilePath "topaz_search_64.exe" -WorkingDirectory "C:\Omega\Omega\" -ArgumentList "--ip PUBLIC IP ADDRESS"
    }
}

#####Create Function to Run Script Indefinitley#####
Function Test-TopazIndefinitely {
    Do {
        Remove-ValuesofPreviouslyStoredVariables
        Test-topaz_new_connect_64
        Test-topaz_search_64
        Test-Topaz_Airships
        Test-Topaz_Alzadaal
        Test-Topaz_Aragoneu
        Test-Topaz_Arrapago
        Test-Topaz_Bastok
        Test-Topaz_Derfland
        Test-Topaz_Dynamis
        Test-Topaz_ELowlands
        Test-Topaz_EUplands
        Test-Topaz_Emptiness
        Test-Topaz_Fauregandi
        Test-Topaz_Gustaberg
        Test-Topaz_Halvung
        Test-Topaz_Jeuno
        Test-Topaz_Kolshushu
        Test-Topaz_Kuzotz
        Test-Topaz_Litelor
        Test-Topaz_Lumoria
        Test-Topaz_Mamool
        Test-Topaz_Misc
        Test-Topaz_Movalpolis
        Test-Topaz_Norvallen
        Test-Topaz_Qufim
        Test-Topaz_Ronfaure
        Test-Topaz_Sandoria
        Test-Topaz_Sarutabaruta
        Test-Topaz_Tavnazian
        Test-Topaz_Tulia
        Test-Topaz_Valdeaunia
        Test-Topaz_Vollbow
        Test-Topaz_Whitegate
        Test-Topaz_Windurst
        Test-Topaz_Zulkheim

        Start-Sleep -Seconds 30
    } While ($True)
}

Remove-ValuesofPreviouslyStoredVariables
Test-TopazIndefinitely